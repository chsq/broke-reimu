var FONT_VH = $('.overlay').height()/100;
var FONT_VW = $('.overlay').width()/100;

var clicker_click_event = new CustomEvent("clicker_click",{"detail":"clickyclick"});
var clock_event = new CustomEvent("clock_tick",{"detail":"tick tock"});
var ready_event = new CustomEvent("game_ready",{"detail":"vars go"});

var drawables = [];
var global_assets = [];
var clicky_thing;
var storeItems;
$.ajax({
  url: "res/characters.json",
  dataType: 'json',
  async: false,
  success: function(data) {
    console.log(data)
    storeItems = data
  }
});

function pre_init_array(len){
  var arr = []
  for(var i = 0; i<len;i++){
    arr.push(0)
  }
  return arr
}

function kFormatter(num) {
    if(num > 999 && num < Math.pow(10,6)){
      return (num/1000).toFixed(1) + 'k'
    }
    else if(num > 999999 && num < Math.pow(10,9)){
      return (num/Math.pow(10,6)).toFixed(1) + 'm'
    }
    else if(num > 999999999 && num < Math.pow(10,12)){
      return (num/Math.pow(10,9)).toFixed(1) + 'b'
    }
    else if(num > 999999999999 && num < Math.pow(10,15)){
      return (num/Math.pow(10,12)).toFixed(1) + 't'
    }
    else if(num > 999999999999999 && num < Math.pow(10,18)){
      return (num/Math.pow(10,15)).toFixed(1) + 'Qd'
    }
    else if(num > 999999999999999999 && num < Math.pow(10,21)){
      return (num/Math.pow(10,18)).toFixed(1) + 'Qt'
    }
    else if(num > 999999999999999999999){ //&& num < Math.pow(10,24)
      return (num/Math.pow(10,21)).toFixed(1) + 'Sext'
    }
    return num.toFixed(1)
}

function getRandomInteger(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

document.addEventListener('game_ready',function(){
  $('.loading-screen').animate({
    opacity:0,
  },500,function(){
    $('.loading-screen').css("display","none");
  });
});

var player = {
  "pow":1,
  "bal":0,
  "items":pre_init_array(storeItems.length),
}

if (typeof(Storage) !== "undefined") {
  if(localStorage.getItem("player")!=null){
    player = JSON.parse(localStorage.getItem("player"));
  }
}


function getParticle(){
  var e = getRandomInteger(0,6)
  if(e==5){
    var img = global_assets[Math.floor(Math.random() * global_assets.length)]
    if(player['items'][parseInt(img.index)] > 0){
      return img
    }
  }
  return false;
}

var particles = [];
var bg_image;
var Particle = function(posx,posy,vel_vec,p){
  this.surface = p;

  this.x = posx;
  this.y = posy;
  this.vel = vel_vec;
  this.isDead = false;
  this.update_rate = 1;
  this.clock = 0;
  this.life=3000;


  this.rotation = Math.round(Math.random()*360);
  this.rotation_speed = Math.round(Math.random()*5);

  this.drag = 0.5;

  this.update = function(){
    if(this.clock >= this.update_rate){
      this.clock = 0;
      this.opacity -= this.fade;
      this.x += this.vel.x;
      this.y += this.vel.y;
      this.life-=1;
      this.rotation += this.rotation_speed
      if(this.x < 0 || this.x > this.surface.width || this.y < 0 || this.y > this.surface.height || this.life == 0){
        this.isDead = true;
      }
    }else{
      this.clock+=1;
    }

  }
}

var ImgParticle = function(x,y,image,p){
    Particle.call(this,x,y,p.createVector(getRandomInteger(-5,5),getRandomInteger(-5,5)),p);
    this.image = image;
    //this.image.resize(0,50)
    this.show = function(){
      this.surface.push()
        this.surface.translate(this.x,this.y)
        this.surface.imageMode(this.surface.CENTER);
        this.surface.angleMode(this.surface.DEGREES);
        this.surface.rotate(this.rotation);
        this.surface.image(this.image, 0, 0,this.image.width/8,this.image.height/8);
      this.surface.pop()
    }
}


var sketch_bg = function(p) {
    p.preload = function(){
      bg_image = p.loadImage('img/hakurei_shrine.png')
    }
    p.setup = function() {
        var canvas = p.createCanvas(p.windowWidth, p.windowHeight);
        canvas.parent('#sketch-bg');
        p.noLoop()
        document.addEventListener("clicker_click",function(){
          var e = getParticle()
          if(e != false){
            particles.push(new ImgParticle(clicky_thing.x,clicky_thing.y,e,p));
            p.loop()
          }
        })
        p.imageMode(p.CENTER);
        bg_image.resize(0,p.height)
    };

    p.draw = function() {
      p.image(bg_image,p.width/2,p.height/2)
        for(i in particles){
          particles[i].show()
          particles[i].update()
          if(particles[i].isDead){
            particles.splice(i,1)

          }
        }
        if(particles.length == 0){
          p.noLoop()
          p.image(bg_image,p.width/2,p.height/2)
        }
    };

    //// DEBUG:
    /*
    p.mouseDragged = function(){
      if(!storeisVisible){
        if(particles.length > 10000){
          particles.splice(0,1)
        }
        particles.push(new ImgParticle(p.mouseX,p.mouseY,img,p));
        p.loop()
      }
    }
    */
};

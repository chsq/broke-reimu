
var clickable = function(size,x,y,img,surface){
  this.x = x;
  this.y = y;
  this.pos = surface.createVector(x,y);
  this.surface = surface
  this.size = size;
  this.clicks = 0;
  this.img = img

  this.max_size = size*1.3;
  this.current_size = size;
  this.shrink_step = 0.5;
  this.grow_step = 20;

  this.shrink_step_mult = 0;

  this.show = function(){

    this.surface.push()
      this.surface.imageMode(this.surface.CENTER)
      this.surface.image(this.img,this.x,this.y,this.current_size,this.current_size)

    this.surface.pop()
  }

  this.update = function(){
    //calculates circle size decrease based on its current size and a modifier
    //ie. bigger circle shrinks faster
    this.shrink_step_mult = (this.current_size/this.max_size)*5
    if(this.current_size > this.size){
      this.current_size -= this.shrink_step*this.shrink_step_mult
    }else{
      this.surface.noLoop()
    }
  }
  this.click = function(){
    document.dispatchEvent(clicker_click_event);
    this.clicks += 1;
    player.bal += 1*player.pow;
    if(this.current_size < this.max_size){
      this.current_size += this.grow_step;
    }
  }
}

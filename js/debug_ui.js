var sketch_debug = function(p) {
  let drawable_count;
  p.setup = function() {
      var canvas = p.createCanvas(p.windowWidth, p.windowHeight);
      canvas.parent('#sketch-debug');

      p.textSize(40);
      p.textAlign(p.CENTER);

    }

  p.draw = function(){
    p.clear();
    //entity count
    drawable_count = drawables.length + particles.length;
    p.text(drawable_count,40,window.innerHeight-40);
    //framerate
    p.text(Math.round(p.frameRate()), window.innerWidth-40, window.innerHeight-40);

  }
}

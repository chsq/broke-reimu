var MENU_ITEM_GAP = 10
var btn_image;
var MENU_ITEM_WIDTH = 0

var MENU_ITEM_HEIGHT = FONT_VH*25
var owned = [];
var current_pos = 0
var old_y;
var storeisVisible = false
var max_height = -(MENU_ITEM_GAP*storeItems.length*2+MENU_ITEM_HEIGHT*storeItems.length)+FONT_VH*100;
//console.log(max_height)
var openZoneTop = FONT_VH*25
var inProgress = false;
var drag = 0.95;
var scroll_accel = 0;
var max_accel = 10
var scroll_accel_c = 0


//debug
var store = []

var menu_button = function(ind,x,y,py,width,height,type,p){
  this.x = x;
  this.y = y
  this.y_relative = y;
  this.width = width;
  this.height = height;
  this.surface = p
  this.parent_y = py
  this.type = type
  this.index = ind;
  this.image =btn_image
  this.fs = 5*FONT_VW;
  this.cost_base = 20 + Math.pow(12,parseInt(this.index)+1)
  this.show = function(){
    this.surface.push();
      this.surface.noStroke();
      this.surface.fill(0,90);
      this.surface.rect(this.x+5,this.y+5,this.width,this.height);

      this.surface.image(this.image,this.x,this.y,this.width,this.height);

      this.surface.fill(255,215,0)
      this.surface.textSize(this.fs);
      this.surface.textAlign(this.surface.CENTER,this.surface.CENTER);
      this.surface.text(this.type + "\n" + kFormatter(Math.round(this.cost_base*Math.pow(storeItems[this.index]['cost_mult'],player.items[this.index])))+"¥",this.x+this.width/2,this.y+this.height/2)

    this.surface.pop();
  }

  this.update = function(){
    this.y = this.y_relative+this.parent_y+current_pos
  }

  this.click = function(x,y){
    if(x > this.x && y > this.y && x < this.x+this.width && y < this.y+this.height){
      //console.log('clicked')
      return true;
    }else{
      return false
    }
  }

}
var menu_element = function(index,img,surface){
  this.index = index
  this.height = MENU_ITEM_HEIGHT
  this.width = MENU_ITEM_WIDTH
  this.surface = surface
  this.img = img
  this.x = MENU_ITEM_GAP;
  this.y = this.index*MENU_ITEM_HEIGHT+MENU_ITEM_GAP*this.index;
  this.name_fs = FONT_VW * 6
  this.img_width = this.width/3;
  this.button_width = this.width/3
  this.button_height = this.height/2
  this.button_top_offset = this.height/4
  this.buttons = []

  this.off_set_i = this.img_width + this.img_width/2
  this.buttons.push(new menu_button(this.index,this.off_set_i,this.button_top_offset,this.y,this.button_width,this.button_height,'buy',this.surface))


  this.a = this.img.width/this.height
  this.show = function(){
    this.surface.push()
      this.surface.noStroke()
      this.surface.fill(86, 3, 25)
      this.surface.rect(this.x,this.y,this.width,this.height)

      this.surface.image(this.img,this.x,this.y,this.img_width,this.height)
      this.surface.fill(255,215,0)
      this.surface.rect(this.x+this.img_width,this.y,FONT_VW*2,this.height)

      this.surface.textSize(this.name_fs);
      this.surface.textAlign(this.surface.CENTER);
      this.surface.fill(218,165,32)
      var ps = " x "+player.items[this.index]
      this.surface.text(storeItems[this.index].name+ps,this.x+this.img_width*2-this.name_fs/2,this.y+this.name_fs);
      var ps2 = kFormatter(player["items"][this.index]*Math.pow(12,this.index)) + "p/s"
      this.surface.textSize(FONT_VW*4);
      this.surface.text(ps2,this.x+this.img_width*2-this.name_fs/2,this.y+this.height-8);
      for(var i in this.buttons){
        this.buttons[i].show()
      }

    this.surface.pop()
  }
  this.update = function(){
    this.y = this.index*MENU_ITEM_HEIGHT+MENU_ITEM_GAP*this.index+current_pos;
    this.button_y = this.y+this.name_fs*2;

    for(var i in this.buttons){
      this.buttons[i].update()
    }
  }

  this.click_check = function(x,y){
    for(var i in this.buttons){
      var btn = this.buttons[i]
      if(btn.click(x,y)){

        if(btn.type=="buy"){
          var item = storeItems[this.index];
          var cost_base = 20 + Math.pow(12,parseInt(this.index)+1)
          var cost = Math.round(cost_base*Math.pow(item['cost_mult'],player.items[this.index]))
          if(player.bal >= cost){
            player.bal -= cost;
            player.items[this.index]+=1;
          }
        }
        document.dispatchEvent(clock_event);
        localStorage.setItem("player",JSON.stringify(player));
      }
    }
  }
}


var sketch_menu = function(p){
  p.preload = function(){
    MENU_ITEM_WIDTH=p.windowWidth-MENU_ITEM_GAP*2
    /*
    $.getJSON("res/characters.json",function(json){
      storeItems = json
    })
    */
    btn_image = p.loadImage('img/btn.png')
    for(var i in storeItems){
      var item = storeItems[i];
      var img = p.loadImage(item['img'])
      var item_entry = new menu_element(i,img,p)
      store.push(item_entry);
      img.index = i;
      global_assets.push(img);

    }

  }
  p.setup = function(){
    p.textFont('Arial')
    var canvas = p.createCanvas(p.windowWidth, p.windowHeight);
    canvas.parent('#sketch-menu');
    FONT_VH = p.windowHeight/100;
    FONT_VW = p.windowWidth/100;
    //console.log(FONT_VH)
    p.noLoop()


    }
    function inOpenZone(y){
      if(-y<openZoneTop){
        return true;
      }
      return false
    }
    function openMenu(){
      inProgress = true
      if(!storeisVisible){
        storeisVisible=true
        p.loop()
        $('.indicator').animate({
          opacity:0,
          top:0
        },1000,function(){})
        $('#sketch-menu').animate({
          opacity:1,
          top:0
        }, 1000, function(){
          particles = []
          inProgress = false
        })
      }
    }
    function closeMenu(){
      inProgress = true
      if(storeisVisible){
        storeisVisible = false
        $('.indicator').animate({
          opacity:1,
          top:($('.overlay').height()/100)*95
        },1000,function(){})
        $('#sketch-menu').animate({
          opacity:0,
          top:$('.overlay').height()
        }, 1000, function(){
          console.log("disabling ------------")
          inProgress = false
          //sconsole.log('hiding')
          current_pos = 0
          $('.indicator').css('display','block')
          //p.noLoop()
        })
      }
    }

  p.draw = function(){
    p.clear()
    p.background(185, 46, 52)
    for(var i in store){
      store[i].show()
      store[i].update()
    }
    //console.log(scroll_accel)
    if(!p.mouseIsPressed){
      scroll_accel = p.constrain(scroll_accel, -10, 10);
      scroll_accel = drag * scroll_accel
      current_pos += scroll_accel
    }


  }
  p.mouseClicked = function(){
    if(storeisVisible){
      for(var i in store){
        store[i].click_check(p.mouseX,p.mouseY);
      }
    }
  }
  p.mouseDragged = function(){
    y = p.mouseY
    console.log(storeisVisible)
    console.log(inProgress)
    if(storeisVisible && !inProgress){
      if(old_y!=undefined){
        if(y > old_y){
          if(current_pos <= 0){
            if(!(scroll_accel < -max_accel)){
              scroll_accel += ((y-old_y)/100)*10;
            }
            current_pos += y-old_y
          }else{
            closeMenu();
            old_y = undefined;
          }
        }else if(y < old_y && current_pos > max_height){
            if(!(scroll_accel > max_accel)){
            scroll_accel += ((y-old_y)/100)*10;
            }
            current_pos += y-old_y
        }
      }
  }
  else if(old_y!=undefined && !inProgress){
    if(y < old_y){
      if(inOpenZone(y) && !storeisVisible){
        openMenu()
        old_y = undefined;
      }
    }
  }
  old_y = y
  };
  p.mouseReleased = function(){
    old_y = undefined;
  }
}

setInterval(update_game,1000);

function update_game(){
  for(var i in player.items){
    var item = storeItems[i];
    var item_pow = Math.pow(12,i)
    player.bal += item_pow*player.items[i];
    document.dispatchEvent(clock_event);

  }
  localStorage.setItem("player",JSON.stringify(player));
}


var f_size = FONT_VW * 15
var sketch_fg = function(p) {

    var clicky_thing_img;
    p.preload = function(){
      clicky_thing_img = p.loadImage('img/donation_box_reimoo2.png')
      bg_music = p.loadSound('audio/bg-music.mp3');
    }
    p.setup = function() {
        bg_music.setVolume(0.1)
        bg_music.loop()

        document.addEventListener("pause", onPause, false);
        document.addEventListener("resume", onResume, false);

        function onPause(){
          console.log("pause")
          bg_music.pause();
        }
        function onResume(){
          console.log("resume")
          bg_music.play();
        }

        var canvas = p.createCanvas(p.windowWidth, p.windowHeight);
        canvas.parent('#sketch-fg');
        p.pixelDensity(1);
        p.textSize(f_size);
        p.textAlign(p.CENTER);
        clicky_thing = new clickable((window.innerHeight/100)*40,window.innerWidth/2,window.innerHeight/1.5,clicky_thing_img,p);
        drawables.push(clicky_thing)
        //player.surface = p;
        document.addEventListener("clock_tick",function(){
          p.redraw()
        })
        p.noLoop()
        document.dispatchEvent(ready_event);
    };

    p.draw = function() {
        //$("canvas")[0].getContext('2d').clearRect(0,0,width,height);
        p.clear();
        p.fill(0);
        p.rect(0,0,p.width,f_size);
        p.fill(255,251,0)
        p.text(kFormatter(player.bal)+"¥",window.innerWidth/2,f_size);
        //p.text(drawables.length+particles.length,40,window.innerHeight-40)


        for(i in drawables){
            drawables[i].update();
            drawables[i].show();
          }


    };
    p.mouseClicked = function(){
        //checks distance between center of clickable and mouse
        //if it is less than the radius of the clickable click is registerd
        if(!storeisVisible){
          var mv = p.createVector(p.mouseX,p.mouseY);
          if(mv.dist(clicky_thing.pos) < clicky_thing.current_size/2){
            clicky_thing.click()
          }
          p.loop()
        }


    }
};


var bgS = new p5(sketch_bg);

var fgS = new p5(sketch_fg);

var mS = new p5(sketch_menu);

//var dbgS = new p5(sketch_debug);
